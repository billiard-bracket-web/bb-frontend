import React, { useState } from "react";
import { Form } from "react-bootstrap";
import PlayerList from "./PlayerList";

import { Button } from "react-bootstrap";

const CreateTournament: React.FC = () => {
  const [amount, setAmount] = useState("8");
  const [stage, setStage] = useState(0);

  const handleNavigation = (value: number) => {
    setStage(stage + value);
  };

  if (stage === 0) {
    return (
      <div>
        <h2>How many players</h2>
        <Form.Group controlId="amountSelect">
          <Form.Control
            as="select"
            value={amount}
            onChange={(e: any) => setAmount(e.target.value)}
          >
            <option value={"8"}>8</option>
            <option value={"16"}>16</option>
            <option value={"32"}>32</option>
            <option value={"64"}>64</option>
          </Form.Control>
        </Form.Group>
        <Button onClick={() => handleNavigation(1)}>Next</Button>
      </div>
    );
  } else {
    return (
      <div>
        <PlayerList amount={amount} />
        <Button onClick={() => handleNavigation(-1)}>Back</Button>
      </div>
    );
  }
};

export default CreateTournament;
