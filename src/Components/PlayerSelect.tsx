import React from "react";
import { Player } from "../App";
import { Form } from "react-bootstrap";

const PlayerSelect: React.FC<IPlayerSelect> = ({
  players,
  selectedPlayer,
  setSelectedPlayer,
  id
}) => {
  let playerList;
  if (players) {
    playerList = players.map(p => (
      <option key={p.id} value={p.id}>
        {p.firstName} {p.lastName}
      </option>
    ));
  } else {
    playerList = <div></div>;
  }
  return (
    <Form.Group controlId={"playerSelect"}>
      <Form.Control
        as="select"
        value={selectedPlayer}
        onChange={(e: any) => setSelectedPlayer(e.target.value, id)}
      >
        <option value={""}>Select player</option>
        {playerList}
      </Form.Control>
    </Form.Group>
  );
};

export default PlayerSelect;

interface IPlayerSelect {
  players?: Player[];
  selectedPlayer: string;
  setSelectedPlayer: any;
  id: number;
}
