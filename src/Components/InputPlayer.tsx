import React, { useState } from "react";
import axios from "axios";
import { Button, Form } from "react-bootstrap";

const InputPlayer: React.FC<{}> = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [tasoitus, setTasoitus] = useState("");
  function addPlayer(
    firstName: string,
    lastName: string,
    tasoitus: string
  ): void {
    if (firstName.length < 1 || lastName.length < 1 || tasoitus.length < 1)
      return;
    axios
      .post("http://localhost:8080/api/v1/player/add", {
        firstName,
        lastName,
        tasoitus
      })
      .then(() => {
        setFirstName("");
        setLastName("");
        setTasoitus("");
      });
  }
  return (
    <Form>
      <h2>Add player</h2>
      <Form.Group controlId="formFirstName">
        <Form.Label>Firstname</Form.Label>
        <Form.Control
          value={firstName}
          onChange={(e: any) => setFirstName(e.target.value)}
        />
      </Form.Group>
      <Form.Group controlId="formLastName">
        <Form.Label>Lastname</Form.Label>
        <Form.Control
          value={lastName}
          onChange={(e: any) => setLastName(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Tasoitus</Form.Label>
        <Form.Control
          value={tasoitus}
          onChange={(e: any) => setTasoitus(e.target.value)}
        />
      </Form.Group>
      <Button
        onClick={() => addPlayer(firstName, lastName, tasoitus)}
        type="submit"
      >
        Add player
      </Button>
    </Form>
  );
};
export default InputPlayer;
