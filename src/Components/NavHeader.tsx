import React from "react";

import { Nav, Navbar } from "react-bootstrap";

const NavHeader: React.FC = () => {
  return (
    <Navbar bg="primary" variant="dark">
      <Nav className="mr-auto">
        <Navbar.Brand href="/">Billiard</Navbar.Brand>
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/player">Add player</Nav.Link>
        <Nav.Link href="/tournament">Tournament</Nav.Link>
        <Nav.Link href="/tournaments">Tournaments</Nav.Link>
      </Nav>
    </Navbar>
  );
};

export default NavHeader;
