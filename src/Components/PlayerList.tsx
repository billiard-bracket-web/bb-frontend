import React, { useState, useEffect } from "react";
import axios from "axios";
import PlayerSelect from "./PlayerSelect";
import { Button } from "react-bootstrap";

const PlayerList: React.FC<IPlayerList> = ({ amount }) => {
  function getPlayers(): void {
    axios
      .get("http://localhost:8080/api/v1/player/")
      .then(res => setPlayers(res.data));
  }
  useEffect(() => {
    getPlayers();
  }, []);

  const [players, setPlayers] = useState<Player[]>();
  const [selectedPlayers, setSelectedPlayers] = useState<string[]>(
    new Array(amount)
  );

  const setSelectedPlayer = (value: string, listId: number) => {
    let tmp = [...selectedPlayers];
    tmp[listId] = value;
    setSelectedPlayers(tmp);
    console.log("value " + value + "list id " + listId);
  };

  const createTournament = (): void => {
    console.log(selectedPlayers);
    axios
      .post("http://localhost:8080/api/v1/tournament/add", selectedPlayers)
      .then(() => {});
  };

  function createSelectList(): React.FC[] {
    let selectList: any[] = [];
    const amountNumber = parseInt(amount);
    for (let i = 0; i < amountNumber; i++) {
      selectList.push(
        <PlayerSelect
          key={i}
          players={players}
          selectedPlayer={selectedPlayers[i]}
          setSelectedPlayer={setSelectedPlayer}
          id={i}
        />
      );
    }
    return selectList;
  }

  return (
    <div>
      <h2>Select players</h2>
      {createSelectList()}
      <Button onClick={() => createTournament()}>Start tournament</Button>
    </div>
  );
};
export default PlayerList;

export interface Player {
  id: number;
  firstName: string;
  lastName: string;
  tasoitus: number;
}

interface IPlayerList {
  amount: string;
}
