import React, { useState, useEffect } from "react";
import { Player } from "../App";
import axios from "axios";
import { ListGroup } from "react-bootstrap";

const TournamentList: React.FC<ITournamentList> = () => {
  const getTournaments = (): void => {
    axios.get("http://localhost:8080/api/v1/tournament/").then(res => {
      setTournaments(res.data);
    });
  };
  useEffect(() => {
    getTournaments();
  }, []);

  const [tournaments, setTournaments] = useState<Tournament[]>([]);

  const tournamentList = (): any => {
    return tournaments.map(t => (
      <ListGroup.Item key={t.id}>
        ID: {t.id}, player amount: {t.players.length}
      </ListGroup.Item>
    ));
  };

  return <ListGroup>{tournamentList()}</ListGroup>;
};
export default TournamentList;

interface ITournamentList {}

interface Tournament {
  id: number;
  players: Player[];
}
