import React from "react";
import "./App.css";
import InputPlayer from "./Components/InputPlayer";
import TournamentList from "./Components/TournamentList";
import CreateTournament from "./Components/CreateTournament";
import NavHeader from "./Components/NavHeader";
import Home from "./Components/Home";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { Container } from "react-bootstrap";

export interface Player {
  id?: number;
  firstName: string;
  lastName: string;
  tasoitus: number;
}

const App: React.FC = () => {
  return (
    <div>
      <NavHeader />
      <Container>
        <Router>
          <Route path="/tournaments" component={TournamentList} />
          <Route path="/tournament" component={CreateTournament} />
          <Route path="/player" component={InputPlayer} />
          <Route path="/" exact={true} component={Home} />
        </Router>
      </Container>
    </div>
  );
};

export default App;
